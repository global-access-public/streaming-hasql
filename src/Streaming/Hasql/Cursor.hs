{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}


module Streaming.Hasql.Cursor where

import Database.Hasql ( DatabaseLog, RunSession(RunSession) )
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session ( QueryError, Session, sql, statement )
import Hasql.Statement ( Statement(..) )
import Pipes.Safe (MonadSafe (Base))
import qualified Pipes.Safe as P
import Protolude hiding (show, toS)
import Protolude.Base (Show (show))
import Protolude.Conv (toS)
import Streaming ( Stream, Of )
import qualified Streaming.Prelude as S



newtype Cursor = Cursor ByteString deriving (IsString)

-- | query to run
newtype Template = Template ByteString deriving (IsString)

-- | a statement to declare a cursor parametrized over some parameters
declareCursor
  :: E.Params a -- ^ paramenters encoding
  -> Cursor -- ^ cursor name
  -> Template -- ^ query template
  -> Statement a ()
declareCursor encoder (Cursor name) (Template template) =
  Statement sql' encoder D.noResult False
  where
    sql' =
      "DECLARE "
        <> name
        <> " NO SCROLL CURSOR FOR "
        <> template

-- | a statement to close the cursor
closeCursor
  :: Cursor -- ^ cursor name
  -> Statement () ()
closeCursor (Cursor name) =
  Statement sql' E.noParams D.noResult True
  where
    sql' = "CLOSE " <> name

-- | number of rows
newtype Batch = Batch Int deriving (Num)

-- | a statement to fetch given number of rows from cursor forward and apply decoders
fetchFromCursor
  :: Cursor -- ^ cursor name
  -> Batch -- ^ max number of rows to fetch
  -> D.Result result -- ^ row decoders
  -> Statement () result
fetchFromCursor (Cursor name) (Batch batch) decoder =
  Statement sql' E.noParams decoder True
  where
    sql' :: ByteString
    sql' =
      "FETCH FORWARD "
        <> do toS $ show batch
        <> " FROM "
        <> name

beginTransaction :: Session ()
beginTransaction = sql "BEGIN TRANSACTION"

endTransaction :: Session ()
endTransaction = sql "END TRANSACTION"

data CursorLog z
  = CursorStarted z
  | CursorFailedToClose z QueryError
  | CursorClosed z
  deriving (Show)

close :: Cursor -> Session ()
close cursor = do
  statement () $ closeCursor cursor
  endTransaction

-- | stream rows for queries of the same template
cursorS
  :: forall z l a m.
  (Show z)
  => MonadSafe m
  => (CursorLog z -> l)
  -> (DatabaseLog -> l)
  -> (CursorLog z -> Base m ())
  -> RunSession m -- ^ execute a session command
  -> E.Params z -- ^ query parameters encoders
  -> D.Row a -- ^ row decoders
  -> Cursor -- ^ desidered cursor name
  -> Template -- ^ query template
  -> Batch -- ^ number of rows to repeat fetching
  -> Stream (Of z) (Stream (Of l) m) () -- ^ params for the next query
  -> Stream (Of a) (Stream (Of l) m) ()
cursorS
  loggerCursor
  loggerDB
  failingCursor
  (RunSession runLDB runBase)
  encoders
  decoders
  cursor
  template
  batch
  params =
    let runOnDB :: Monoid x => Session x -> Stream (Of a) (Stream (Of l) m) (Either QueryError x)
        runOnDB = lift . S.map loggerDB . runLDB
     in S.for params $
          \parameters -> do
            lift . S.yield $ loggerCursor $ CursorStarted parameters
            r <- runOnDB do
              beginTransaction
              statement parameters $ declareCursor encoders cursor template
            case r of
              Left _  -> pure ()
              Right _ -> do
                    releaser <- lift . lift $
                      P.register $ do
                        void $ runBase $ close cursor
                        failingCursor $ CursorClosed parameters
                    let loop = do
                          mxs <-
                            runOnDB $
                              statement () $
                                fetchFromCursor cursor batch $
                                  D.rowList decoders
                          case mxs of
                            Right [] -> pure ()
                            Right xs -> S.each xs >> loop
                            Left _ -> pure ()
                    loop
                    cr <-  runOnDB $ close cursor
                    case cr of
                        Left e  -> lift . S.yield $ loggerCursor $ CursorFailedToClose parameters e
                        Right () -> lift . S.yield $ loggerCursor $ CursorClosed parameters
                    lift . lift $ P.release releaser


