{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Database.Hasql where

import Hasql.Connection (ConnectionError, Settings, acquire)
import qualified Hasql.Connection as HC
import Hasql.Session (QueryError, Session, run)
import Pipes.Safe (Base, MonadSafe)
import qualified Pipes.Safe as P
import Protolude
import Streaming hiding (run)
import qualified Streaming.Prelude as S

data DatabaseLog
  = ConnectionReady
  | ConnectionClosed
  | ConnectionFailed ConnectionError
  | QueryFailed QueryError
  | QueryImpossible ConnectionError
  deriving (Show, Eq)

-- newtype DBException = DBException DatabaseLog deriving (Show, Eq, Generic, Exception)

-- newtype RunSession = RunSession (forall a. Session a -> IO a)

-- connect :: (forall m. MonadIO m => DatabaseLog -> m ()) -> Settings -> SafeT IO RunSession
-- connect tracer settings' = do
--   connection <- liftIO (acquire settings') >>= failEither (DBException . ConnectionFailed)
--   tracer ConnectionReady
--   void $
--     P.register $ do
--       liftIO $ HC.release connection
--       tracer ConnectionClosed
--   pure $ RunSession \s ->
--     run s connection >>= failEither (DBException . QueryFailed)

-- failEither :: (MonadThrow m, Exception e) => (t -> e) -> Either t a -> m a
-- failEither q (Left dbf) = throwM $ q dbf
-- failEither _q (Right x) = pure x

type LoggedDB m = Stream (Of DatabaseLog) m

data RunSession m = RunSession
  { runSession_ldb :: forall a. Session a -> LoggedDB m (Either QueryError a)
  , runSession_base :: forall a.  Session a -> Base m (Either QueryError a)
  }

connectS :: (MonadSafe m) => (DatabaseLog -> Base m ()) -> Settings -> LoggedDB m (Maybe (RunSession m))
connectS warner settings' = do
  mconnection <- liftIO (acquire settings')
  case mconnection of
    Left failed -> do
      S.yield $ ConnectionFailed failed
      pure Nothing
    Right connection -> do
      S.yield ConnectionReady
      void $
        lift $
          P.register $ do
            liftIO $ HC.release connection
            warner ConnectionClosed
      pure $ Just $ RunSession
        do
          \s ->
            liftIO (run s connection) >>= either
              do
                \e -> do
                  S.yield . QueryFailed $ e
                  pure $ Left e 
              do pure . Right
        do
          \s -> do
            liftIO (run s connection) >>= either
              do
                \e -> do
                  warner . QueryFailed $ e
                  pure $ Left e 
              do pure . Right 
